import Vue from 'vue'
// import Vuex from 'vuex'
import Vuex from '../kvuex/kvuex.js';

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    count: 0
  },
  mutations: {
    add(state) {
      return state.count++;
    }
  },
  actions: {
    add({commit}) {
      setTimeout(() => {
        commit('add');
      }, 1000)
    }
  },
  getters: {
    doubleCounter(state) {
      return state.count * 2
    }
  },
  modules: {
  }
})
