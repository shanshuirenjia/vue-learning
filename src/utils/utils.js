export const forEachObject = (object, callback) => {
  Object.keys(object).forEach(key => callback(key, object[key]));
};