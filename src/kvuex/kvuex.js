let _Vue;

function forEachObject(object, callback) {
  Object.keys(object).forEach(key => callback(key, object[key]));
}

class Store {

  constructor(options) {
    let { state = {}, getters = {}, mutations = {}, actions = {} } = options;

    this.mutations = {};
    this.actions = {};
    this.getters = {};
    // data需要具备双向绑定的功能
    this._vm = new _Vue({ // 借鸡生蛋, 使用Vue 构造函数，使数据具备双向绑定功能
      data() {
        return {
          $$state: state  // 不希望被代理，就添加上$, 内部约定
        };
      }
    });
    // init mutations
    forEachObject(mutations, (key, fn) => {
      this.mutations[key] = fn;
    });

    // init actions
    forEachObject(actions, (key, fn) => {
      this.actions[key] = fn;
    });

    // init getters
    // 暗号：天王盖地虎
    forEachObject(getters, (key, fn) => {
      Object.defineProperty(this.getters, key, {
        get: () => {
          return fn(this.state)
        }
      });
    });
  }

  get state() { // 向外提供state 获取接口
    return this._vm._data.$$state;
  }

  set state(v) {
    console.error('please use replaceState to reset state');
  }

  // 更新数据的接口函数
  commit = (type, payload) => {
    const entry = this.mutations[type];
    if (!entry) {
      console.log("unknown mutation");
    }
    entry && entry(this.state, payload);
  }

  // 更新数据的接口函数
  dispatch = (type, payload) => {
    const entry = this.actions[type];
    if (!entry) {
      console.log("unknown action");
    }
    // 此处传this, 便于 dispath 执行好后，继续执行commit
    entry && entry(this, payload);
  }

}


function install(Vue) {
  _Vue = Vue;

  Vue.mixin({
    beforeCreate() {
      if (this.$options && this.$options.store) {
        Vue.prototype.$store = this.$options.store;
      }
    }
  })
}


export default { Store, install }