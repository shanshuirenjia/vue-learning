import Vue from 'vue';
import Notification from '@/components/notice/Notice.vue';

/** 
 * 作业：通过Vue.extend()接口函数，实现一个消息组件的封装
 * 
 * 分析：弹窗类组件一般为定位布局，其dom节点挂在的位置不会影响其最终结果
 * 结论：将组件直接挂在在body上，与app同级，便于管理，且不会影响原来的dom结构
 * 
 * 方案1: 杨老师的实现方式是：首先通过Vue构造函数的方式，创建一个实例；然后将实例的dom节点挂在在body上
 * 方案2: 通过Vue.extend() 接口函数实现
*/

// 暗号：老杨喊我来搬砖

// 通过基础构造器生成一个子类
const NotificationContructor = Vue.extend(Notification);

const create = (options) => {
  // 实例化子类
  const notification = new NotificationContructor({propsData: options});
  // 调用生命周期函数, 生成虚拟节点
  notification.vm = notification.$mount();
  // 将虚拟节点-->的dom内容到body中
  document.body.appendChild(notification.vm.$el);

  // 绑定删除事件
  notification.vm.remove = () => {
    document.body.removeChild(notification.vm.$el);
  }

  return notification.vm;
}

export default create;
