function proxy(vm, key) {
  Object.keys(vm[key]).forEach(k => {
    Object.defineProperty(vm, k, {
      get() {
        return vm[key][k];  // 获取是，通过代理获取原始值
      },
      set(v) {
        vm[key][k] = v; // 设置时，直接设置原始值
      }
    })
  })
}

class KVue {
  
  constructor(options) {
    this.$options = options;
    this.$data = options.data;
    this.methods = options.methods;

    // observe $data
    observe(this.$data);

    // proxy $data, 在外面访问时，可以直接使用 实例.属性 的方式访问vm实例的数据
    proxy(this, '$data');

    // compiler template
    new Compiler("#app", this);
  }

}
