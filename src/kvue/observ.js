
function defineReactive(obj, key, val) {

  // 如果val 也是对象，需要递归将其也设置为响应式数据
  observe(val);

  const dep = new Dep();

  // 实现 obj 的 key 为响应式数据
  Object.defineProperty(obj, key, {
    get() {
      console.log('get', key)
      Dep.target && dep.addDep(Dep.target);
      return val;
    },
    set(newVal) {
      if (val !== newVal) {
        // if newVal is obj: this.abc = {name: 'abc'};
        observe(newVal); 
        val = newVal;
        console.log('set', key, val)
        dep.notify();
      }
    }
  });
}

class Observer {

  constructor(value) {
    this.value = value;
    this.walk(value);
  }
  
  walk(obj) {
    Object.keys(obj).forEach(key => {
      defineReactive(obj, key, obj[key]);
    });
  }

}


function observe(obj) {
  if (typeof obj !== 'object' || obj === null) {
    return obj;
  }

  new Observer(obj);
}
