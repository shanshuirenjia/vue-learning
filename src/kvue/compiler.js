class Compiler {
  
  constructor(el, vm) {
    this.$el = document.querySelector(el);
    this.$vm = vm;

    if (this.$el) {
      this.compiler(this.$el);
    }
  }

  compiler(el) {
    // 解析模版
    el.childNodes.forEach(node => {
      if (node.nodeType === 1) {  // element
        // 标签处理
        this.compilerElement(node);
      } else if (node.nodeType === 3 && this.isInter(node)) {  // 文本内容
        // 文本处理
        this.compilerText(node);
      }

      if (node.childNodes && node.childNodes.length > 0) {
        this.compiler(node);
      }
    });
  }

  isInter(node) {
    return /\{\{(.*)\}\}/.test(node.textContent);
  }

  isCommonDir(dirName) {
    return dirName.indexOf('k-') === 0;  'k- 开头表示命令 类似 v- 开头'
  }

  isEventDir(dirName) {
    return dirName.indexOf('@') === 0;  '@ 开头表示事件'
  }

  compilerText(node) {
    this.update(node, RegExp.$1, 'text');
  }

  compilerElement(node) {
    const { attributes } = node;
    Array.from(attributes).forEach(attr => {
      const { name: dirName, value: key } = attr;
      if (this.isCommonDir(dirName)) {
        const dir = dirName.substring(2);
        this[dir] && this[dir](node, key);
      } else if (this.isEventDir(dirName)) {
        // 处理 “@” 指令
        const dir = dirName.substring(1);
        this.bindEvent(node, key, dir);
      }
    })
  }

  update(node, key, dir) {
    let fn = this[dir + 'Updater'];

    fn && fn(node, this.$vm[key]);

    new Watcher(this.$vm, key, (val) => {
      fn && fn(node, val)
    });
  }

  textUpdater(node, val) {
    node.textContent = val;
  }

  text(node, key) {
    this.update(node, key, 'text');
  }

  bindEvent(node, key, dir) {
    const eventType = dir;
    const cb = this.$vm.methods[key];

    if (eventType && cb) {
      node.addEventListener(eventType, cb.bind(this.$vm), false);
    }
  }

  modelUpdater(node, value) {
    // 更新 node 的值
    node.value = typeof value === 'undefined' ? '' : value; 
  }

  model(node, key) {
    // 取出model值
    let val = this.$vm.key;
    let fn = this['modelUpdater'];

    fn && fn(node, val);

    new Watcher(this.$vm, key, (val) => {
      fn && fn(node, val);
    });

    node.addEventListener('input', (e) => {
      let newValue = e.target.value;
      if (val === newValue) {
        return;
      }

      this.$vm[key] = newValue;
      val = newValue;
    });
  }
}