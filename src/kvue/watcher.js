class Watcher {
  constructor(vm, key, updateFn) {
    this.vm = vm;
    this.key = key;
    this.updateFn = updateFn;

    Dep.target = this;
    this.vm[key];
    Dep.target = null;
  }

  update() {
    // this.vm 当前组件实例
    // data 更新后, this.vm[key] 也会更新（闭包）
    // 使用新的值更新UI
    this.updateFn.call(this.vm, this.vm[this.key]);
  }
}